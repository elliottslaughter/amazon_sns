This is a simple Python script that sends an SMS to a phone number
using Amazon SNS. It is appropriate for a single user sending
occaisional SMS messages to a single phone number. If your
requirements are much more complicated than that, you will probably
want to write your own, though the script hopefully demonstrates how
to use the API.

# Prerequisites

  * Python 2.7
  * Boto (https://github.com/boto/boto)
  * An Amazon AWS account with a IAM sub-account with permissions for SNS
      * Download the credentials in Amazon's CSV format and name the
        file as `aws_sns_credentials.csv`

# License

Copyright (c) 2014, Elliott Slaughter <elliottslaughter@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
