#!/usr/bin/env python

import argparse, boto.sns, csv

def send_sms(aws_access_key_id, aws_secret_access_key,
             topic_id, topic_name, endpoint, message):
    conn = boto.sns.connect_to_region(
        'us-east-1',
        aws_access_key_id = aws_access_key_id,
        aws_secret_access_key = aws_secret_access_key)

    topic = conn.create_topic(topic_id)[u'CreateTopicResponse'] \
            [u'CreateTopicResult'][u'TopicArn']

    conn.set_topic_attributes(topic, 'DisplayName', topic_name)

    subscriptions = conn.get_all_subscriptions_by_topic(topic) \
                    [u'ListSubscriptionsByTopicResponse'] \
                    [u'ListSubscriptionsByTopicResult'][u'Subscriptions']

    subscribed = False
    for subscription in subscriptions:
        if subscription[u'Endpoint'] == endpoint:
            subscribed = True
        else:
            conn.unsubscribe(subscription[u'SubscriptionArn'])

    if not subscribed:
        conn.subscribe(topic, 'sms', endpoint)

    conn.publish(topic = topic, message = message)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description = 'Send SMS to a phone number.')
    parser.add_argument('phone_number')
    parser.add_argument('message')
    args = parser.parse_args()

    with open('aws_sns_credentials.csv', 'rb') as f:
        _, aws_access_key_id, aws_secret_access_key = list(csv.reader(f))[1]
    send_sms(
        aws_access_key_id = aws_access_key_id,
        aws_secret_access_key = aws_secret_access_key,
        topic_id = 'message_topic',
        topic_name = 'example',
        endpoint = args.phone_number,
        message = args.message)
